﻿using api.unisex_fashion.Business.Service.Contract;
using api.unisex_fashion.Data.Entity.Model;
using Microsoft.AspNetCore.Mvc;

namespace api.unisex_fashion.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        /// <summary>
        ///  Le service de gestion des unités de mesure
        /// </summary>
        private readonly IProductService _productService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProductController"/> class.
        /// </summary>
        /// <param name="productService">The unite service.</param>
        public ProductController(IProductService productService)
        {
            _productService = productService;
        }

        // GET api/Unites
        /// <summary>
        /// Ressource pour récupérer la liste des unités de mesure.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(List<Product>), 200)]
        public async Task<ActionResult> GetUnitesAsync()
        {
            var products = await _productService.GetProductsAsync().ConfigureAwait(false);

            return Ok(products);
        }

        // POST api/Prodcts
        /// <summary>
        /// Ressource pour créer une nouvelle unité de mesure.
        /// </summary>
        /// <param name="product">les données de l'unité à créer</param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(Product), 200)]
        public async Task<ActionResult> CreateProductAsync([FromBody] Product product)
        {
            if (string.IsNullOrWhiteSpace(product.ProductName))
            {
                return Problem("Echec : nous avons un nom d'unité de mesure vide !!");
            }

            try
            {
                var productAdded = await _productService.CreateProductAsync(product).ConfigureAwait(false);

                return Ok(productAdded);
            }
            catch (Exception e)
            {
                return BadRequest(new
                {
                    Error = e.Message,
                });
            }

        }

        // PUT api/Unites/1
        /// <summary>
        /// Ressource pour mettre à jour une unité de mesure.
        /// </summary>
        /// <param name="ProductId">L'identifiant de l'unité.</param>
        /// <param name="product">les données modifiées.</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [ProducesResponseType(typeof(Product), 200)]
        public async Task<ActionResult> UpdateProductAsync(int productId, [FromBody] Product product)
        {
            if (string.IsNullOrWhiteSpace(product.ProductName))
            {
                return Problem("Echec : nous avons un nom d'unité de mesure vide !!");
            }

            try
            {
                var productUpdated = await _productService.UpdateProductAsync(productId, product).ConfigureAwait(false);

                return Ok(productUpdated);
            }
            catch (Exception e)
            {
                return BadRequest(new
                {
                    Error = e.Message,
                });
            }

        }

        // DELETE api/Unites/1
        /// <summary>
        /// Ressource pour supprimer une unité de mesure.
        /// </summary>
        /// <param name="id">L'identifiant de l'unité.</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(Product), 200)]
        public async Task<ActionResult> DeleteUniteAsync(int productId)
        {
            try
            {
                var productDeleted = await _productService.DeleteProductAsync(productId).ConfigureAwait(false);

                return Ok(productDeleted);
            }
            catch (Exception e)
            {
                return BadRequest(new
                {
                    Error = e.Message,
                });
            }

        }

    }
}
