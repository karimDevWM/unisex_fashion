﻿using api.unisex_fashion.Business.Service.Contract;
using api.unisex_fashion.Data.Entity.Model;
using api.unisex_fashion.Data.Repository.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace api.unisex_fashion.Business.Service
{
    public class ProductService : IProductService
    {
        private readonly IProductRepository _productRepository;

        public ProductService(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public async Task<List<Product>> GetProductsAsync()
        {
            var products = await _productRepository.GetProductsAsync().ConfigureAwait(false);
            List<Product> producte = new(products.Count);

            foreach (var product in products)
            {
                producte.Add(product);
            }

            return producte;
        }

        public async Task<Product> CreateProductAsync(Product product)
        {
            var isExists = await CheckProductNameExisteAsync(product.ProductName).ConfigureAwait(false);
            if (isExists)
                throw new Exception("il existe dejà un product name du même nom !");

            var productAdded = await _productRepository.CreateProductAsync(product).ConfigureAwait(false);

            return productAdded;
        }

        public async Task<Product> UpdateProductAsync(int idProduct, Product product)
        {
            var isExists = await CheckProductNameExisteAsync(product.ProductName).ConfigureAwait(false);
            if(isExists)
                throw new Exception("il existe dejà un product name du même nom !");

            var productGet = await _productRepository.GetProductByIdAsync(idProduct).ConfigureAwait(false);
            if (productGet == null)
                throw new Exception($"Il n'existe aucune unité de mesure avec cet identifiant : {idProduct}");

            productGet.ProductName = product.ProductName;

            var productUpdated = await _productRepository.UpdateProductAsync(product).ConfigureAwait(false);

            return productUpdated;
        }

        public async Task<Product> DeleteProductAsync(int idProduct)
        {
            var productGet = await _productRepository.GetProductByIdAsync(idProduct).ConfigureAwait(false);
            if (productGet == null)
                throw new Exception($"Il n'existe aucune unité de mesure avec cet identifiant : {idProduct}");

            var productDeleted = await _productRepository.DeleteProductAsync(productGet).ConfigureAwait(false);

            return productDeleted;
        }

        private async Task<bool> CheckProductNameExisteAsync(string productName)
        {
            var productGet = await _productRepository.GetProductByNameAsync(productName).ConfigureAwait(false);

            return productGet != null;
        }
    }
}
