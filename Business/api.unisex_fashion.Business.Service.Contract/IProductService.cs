﻿using api.unisex_fashion.Data.Entity.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace api.unisex_fashion.Business.Service.Contract
{
    public interface IProductService
    {
        Task<List<Product>> GetProductsAsync();

        Task<Product> CreateProductAsync(Product product);

        Task<Product> UpdateProductAsync(int idProduct, Product product);

        Task<Product> DeleteProductAsync(int idProduct);
    }
}
