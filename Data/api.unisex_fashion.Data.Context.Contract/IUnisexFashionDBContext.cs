﻿using api.unisex_fashion.Data.Entity.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace api.unisex_fashion.Data.Context.Contract
{
    public interface IUnisexFashionDBContext : IDbContext
    {
        DbSet<Cart> Carts { get; }
        DbSet<Category> Categories { get; }
        DbSet<Commande> Commandes { get; }
        DbSet<Commandproduct> Commandproducts { get; }
        DbSet<Product> Products { get; }
        DbSet<Productpanier> Productpaniers { get; }
        DbSet<Role> Roles { get; }
        DbSet<Stockproduct> Stockproducts { get; }
        DbSet<Taille> Tailles { get; }
        DbSet<User> Users { get; }
    }
}
