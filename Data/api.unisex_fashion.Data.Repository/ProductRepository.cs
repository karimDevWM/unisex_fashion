﻿using api.unisex_fashion.Data.Context.Contract;
using api.unisex_fashion.Data.Entity.Model;
using api.unisex_fashion.Data.Repository.Contract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace api.unisex_fashion.Data.Repository
{
    public class ProductRepository : IProductRepository
    {
        private readonly IUnisexFashionDBContext _dbContext;

        public ProductRepository(IUnisexFashionDBContext unisexFashionDBContext)
        {
            _dbContext = unisexFashionDBContext;
        }

        public async Task<Product> CreateProductAsync(Product product)
        {
            var elementAdded = await _dbContext.Products.AddAsync(product).ConfigureAwait(false);
            await _dbContext.SaveChangesAsync().ConfigureAwait(false);
            return elementAdded.Entity;
        }

        public async Task<List<Product>> GetProductsAsync()
        {
            var elements = await _dbContext.Products.ToListAsync();
            return elements;
        }

        public async Task<Product> GetProductByIdAsync(int id)
        {
            var element = await _dbContext.Products.FirstOrDefaultAsync(product => product.ProductId == id);
            return element;
        }

        public async Task<Product> GetProductByNameAsync(string name)
        {
            var element = await _dbContext.Products.FirstOrDefaultAsync(product => product.ProductName == name);
            return element;
        }

        public async Task<Product> UpdateProductAsync(Product product)
        {
            var elementUpdated = _dbContext.Products.Update(product);
            await _dbContext.SaveChangesAsync().ConfigureAwait (false);
            return elementUpdated.Entity;
        }

        public async Task<Product> DeleteProductAsync(Product product)
        {
            var elementDeleted = _dbContext.Products.Remove(product);
            return elementDeleted.Entity;
        }
    }
}
