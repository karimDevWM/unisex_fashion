﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using api.unisex_fashion.Data.Entity.Model;
using api.unisex_fashion.Data.Context.Contract;

namespace api.unisex_fashion.Data.Entity
{
    public partial class UnisexFashionDBContext : DbContext, IUnisexFashionDBContext
    {
        public UnisexFashionDBContext()
        {
        }

        public UnisexFashionDBContext(DbContextOptions<UnisexFashionDBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Cart> Carts { get; set; } = null!;
        public virtual DbSet<Category> Categories { get; set; } = null!;
        public virtual DbSet<Commande> Commandes { get; set; } = null!;
        public virtual DbSet<Commandproduct> Commandproducts { get; set; } = null!;
        public virtual DbSet<Product> Products { get; set; } = null!;
        public virtual DbSet<Productpanier> Productpaniers { get; set; } = null!;
        public virtual DbSet<Role> Roles { get; set; } = null!;
        public virtual DbSet<Stockproduct> Stockproducts { get; set; } = null!;
        public virtual DbSet<Taille> Tailles { get; set; } = null!;
        public virtual DbSet<User> Users { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseMySql("server=localhost;database=unisex_fashion;port=3306;user=root;password=root", Microsoft.EntityFrameworkCore.ServerVersion.Parse("5.7.31-mysql"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.UseCollation("latin1_swedish_ci")
                .HasCharSet("latin1");

            modelBuilder.Entity<Cart>(entity =>
            {
                entity.ToTable("cart");

                entity.HasIndex(e => e.ProductPanierId, "fk_ProductPanierId");

                entity.HasIndex(e => e.UserId, "fk_UserId");

                entity.Property(e => e.CartId).HasColumnType("int(11)");

                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.ProductPanierId).HasColumnType("int(11)");

                entity.Property(e => e.UserId).HasColumnType("int(11)");
            });

            modelBuilder.Entity<Category>(entity =>
            {
                entity.ToTable("category");

                entity.Property(e => e.CategoryId).HasColumnType("int(11)");

                entity.Property(e => e.CategoryName)
                    .HasMaxLength(500)
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");
            });

            modelBuilder.Entity<Commande>(entity =>
            {
                entity.ToTable("commande");

                entity.HasIndex(e => e.CommandProductId, "CommandProductId");

                entity.HasIndex(e => e.UserId, "userId");

                entity.Property(e => e.CommandeId)
                    .HasColumnType("int(11)")
                    .HasColumnName("commandeId");

                entity.Property(e => e.CommandProductId).HasColumnType("int(11)");

                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.UserId)
                    .HasColumnType("int(11)")
                    .HasColumnName("userId");
            });

            modelBuilder.Entity<Commandproduct>(entity =>
            {
                entity.ToTable("commandproduct");

                entity.HasIndex(e => e.ProductId, "fk_ProductId");

                entity.HasIndex(e => e.CommandeId, "fk_commandeId");

                entity.Property(e => e.CommandProductId).HasColumnType("int(11)");

                entity.Property(e => e.CommandeId)
                    .HasColumnType("int(11)")
                    .HasColumnName("commandeId");

                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.ProductId)
                    .HasColumnType("int(11)")
                    .HasColumnName("productId");
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.ToTable("product");

                entity.HasIndex(e => e.ProductCategoryId, "ProductCategoryId");

                entity.Property(e => e.ProductId).HasColumnType("int(11)");

                entity.Property(e => e.ProductCategoryId).HasColumnType("int(11)");

                entity.Property(e => e.ProductName)
                    .HasMaxLength(500)
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");
            });

            modelBuilder.Entity<Productpanier>(entity =>
            {
                entity.ToTable("productpanier");

                entity.HasIndex(e => e.CartId, "fk_CartId");

                entity.HasIndex(e => e.ProductId, "fk_ProductId");

                entity.Property(e => e.ProductPanierId).HasColumnType("int(11)");

                entity.Property(e => e.CartId).HasColumnType("int(11)");

                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.ProductId).HasColumnType("int(11)");

                entity.Property(e => e.Quantity)
                    .HasColumnType("int(11)")
                    .HasColumnName("quantity");
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.ToTable("role");

                entity.Property(e => e.RoleId).HasColumnType("int(11)");

                entity.Property(e => e.RoleName)
                    .HasMaxLength(500)
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");
            });

            modelBuilder.Entity<Stockproduct>(entity =>
            {
                entity.ToTable("stockproduct");

                entity.HasIndex(e => e.ProductId, "ProductId");

                entity.HasIndex(e => e.TailleId, "tailleId");

                entity.Property(e => e.StockProductId).HasColumnType("int(11)");

                entity.Property(e => e.AvailableQuantity)
                    .HasColumnType("int(11)")
                    .HasColumnName("available_quantity")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.ProductId).HasColumnType("int(11)");

                entity.Property(e => e.Quantity)
                    .HasColumnType("int(11)")
                    .HasColumnName("quantity");

                entity.Property(e => e.TailleId)
                    .HasColumnType("int(11)")
                    .HasColumnName("tailleId");
            });

            modelBuilder.Entity<Taille>(entity =>
            {
                entity.ToTable("taille");

                entity.Property(e => e.TailleId).HasColumnType("int(11)");

                entity.Property(e => e.TailleName).HasMaxLength(50);
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("user");

                entity.HasIndex(e => e.UserRoleId, "UserRoleId");

                entity.Property(e => e.UserId).HasColumnType("int(11)");

                entity.Property(e => e.UserEmail)
                    .HasMaxLength(500)
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.UserName)
                    .HasMaxLength(500)
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.UserPassword)
                    .HasMaxLength(500)
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.UserRoleId).HasColumnType("int(11)");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
