﻿using System;
using System.Collections.Generic;

namespace api.unisex_fashion.Data.Entity.Model
{
    public partial class Taille
    {
        public int TailleId { get; set; }
        public string? TailleName { get; set; }
    }
}
