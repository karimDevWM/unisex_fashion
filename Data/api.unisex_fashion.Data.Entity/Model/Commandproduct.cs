﻿using System;
using System.Collections.Generic;

namespace api.unisex_fashion.Data.Entity.Model
{
    public partial class Commandproduct
    {
        public int CommandProductId { get; set; }
        public int? ProductId { get; set; }
        public int? CommandeId { get; set; }
        public DateTime? CreatedAt { get; set; }
    }
}
