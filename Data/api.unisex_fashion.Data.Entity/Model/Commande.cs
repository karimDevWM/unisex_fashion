﻿using System;
using System.Collections.Generic;

namespace api.unisex_fashion.Data.Entity.Model
{
    public partial class Commande
    {
        public int CommandeId { get; set; }
        public int? UserId { get; set; }
        public int? CommandProductId { get; set; }
        public DateTime? CreatedAt { get; set; }
    }
}
