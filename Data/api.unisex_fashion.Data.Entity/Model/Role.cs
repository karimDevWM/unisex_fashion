﻿using System;
using System.Collections.Generic;

namespace api.unisex_fashion.Data.Entity.Model
{
    public partial class Role
    {
        public int RoleId { get; set; }
        public string? RoleName { get; set; }
    }
}
