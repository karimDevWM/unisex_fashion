﻿using System;
using System.Collections.Generic;

namespace api.unisex_fashion.Data.Entity.Model
{
    public partial class User
    {
        public int UserId { get; set; }
        public string? UserName { get; set; }
        public string? UserEmail { get; set; }
        public string? UserPassword { get; set; }
        public int? UserRoleId { get; set; }
    }
}
