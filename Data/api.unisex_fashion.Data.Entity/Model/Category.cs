﻿using System;
using System.Collections.Generic;

namespace api.unisex_fashion.Data.Entity.Model
{
    public partial class Category
    {
        public int CategoryId { get; set; }
        public string? CategoryName { get; set; }
    }
}
