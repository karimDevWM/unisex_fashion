﻿using System;
using System.Collections.Generic;

namespace api.unisex_fashion.Data.Entity.Model
{
    public partial class Cart
    {
        public int CartId { get; set; }
        public int? UserId { get; set; }
        public int? ProductPanierId { get; set; }
        public DateTime? CreatedAt { get; set; }
    }
}
