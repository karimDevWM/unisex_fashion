﻿using System;
using System.Collections.Generic;

namespace api.unisex_fashion.Data.Entity.Model
{
    public partial class Stockproduct
    {
        public int StockProductId { get; set; }
        public int? ProductId { get; set; }
        public int? TailleId { get; set; }
        public int? Quantity { get; set; }
        public int? AvailableQuantity { get; set; }
    }
}
