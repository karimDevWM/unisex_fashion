﻿using System;
using System.Collections.Generic;

namespace api.unisex_fashion.Data.Entity.Model
{
    public partial class Productpanier
    {
        public int ProductPanierId { get; set; }
        public int? ProductId { get; set; }
        public int? CartId { get; set; }
        public int? Quantity { get; set; }
        public DateTime? CreatedAt { get; set; }
    }
}
